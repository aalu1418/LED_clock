#include <DS3231.h>
#include <Time.h>
#include <TimeLib.h>

#define FASTLED_ALLOW_INTERRUPTS0
#include <FastLED.h>

#define DATA_PIN 6
#define NUM_LEDS 25
#define TIME_HEADER  "T"   // Header tag for serial time sync message
#define TIME_REQUEST  7    // ASCII bell character requests a time sync message 

DS3231 rtc(SDA, SCL); // Pin A4: SDA, Pin A5: SCL
CRGB leds[NUM_LEDS];
uint8_t numbers[] = {119, 68, 107, 110, 92, 62, 63, 100, 127, 126}; // binary representations for LEDs
int last_h, last_m; // used for tracking time updates
bool is_day;
Time rtc_time;
bool offset_set;

// numbers for sunrise and sunset from http://aa.usno.navy.mil/cgi-bin/aa_rstablew.pl?ID=AA&year=2018&task=0&state=MI&place=Ann+Arbor
// to calibrate for daylight savings: add 1 to hour (from 2nd Sunday in March to 1st Sunday in November)
long sunrise[] = {28860, 27060, 24420, 21240, 18840, 17880, 18720, 20520, 22500, 24480, 26760, 28620};
long sunset[] = {62880, 65220, 67320, 69420, 71400, 72780, 72600, 70560, 67500, 64380, 61980, 61380};

// color options: http://fastled.io/docs/3.1/struct_c_r_g_b.html
CRGB choose_color() {
  String date = rtc.getDateStr();
  int mth = date.substring(3,5).toInt() - 1; // zero-index the month
  CRGB color = CRGB(0, 190, 255); // at night

  int time_in_seconds = last_h * 3600 + last_m * 60;
  // during the day
  /*if (time_in_seconds >= int(sunrise[mth]) && time_in_seconds <= int(sunset[mth])) {
    color = CRGB::LavenderBlush;
  }*/

  return color;
}

void set_leds(int num, int offset) {
  for (int i = 0; i < 7; i++) {
    bool light = (numbers[num] >> i) & 0x01;
    if (light)
      leds[offset+i] = choose_color();
    else
      leds[offset+i] = CRGB::Black;
  }
  FastLED.show();

  return;
}

void update_time(int hh, int mm) {
//  if (hh >= 12) {
//    is_day = false;
//  }
//  else {
//    is_day = true;
//  }

  // if hh >= 10 manually set the tens digit
  hh = (hh%12 == 0) ? 12 : (hh % 12);
  if (hh >= 10) {
    leds[0] = choose_color();
    leds[1] = choose_color();
  }
  else {
    leds[0] = CRGB::Black;
    leds[1] = CRGB::Black;
  }
    set_leds(hh%10, 2);
    set_leds(mm/10, 11);
    set_leds(mm%10, 18);
  
  return;
}

/* function used to sync time with computer via Processing */
time_t requestSync()
{
  Serial.write(TIME_REQUEST);  
  return 0; // the time will be sent later in response to serial mesg
}

/* function used to sync time with computer via Processing */
void processSyncMessage() {
  unsigned long pctime;
  const unsigned long DEFAULT_TIME = 1357041600; // Jan 1 2013

  if(Serial.find(TIME_HEADER)) {
     pctime = Serial.parseInt();
     if(pctime >= DEFAULT_TIME) { // check the integer is a valid time (greater than Jan 1 2013)
       Serial.print(String(pctime));
       setTime(pctime); // Sync Arduino clock to the time received on the serial port
     }
  }
}

void setup() { 
  Serial.begin(9600);
  rtc.begin();
  
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS); 
  FastLED.setBrightness(255); // brightness can be 0-255

  // set ":" lights
  leds[9] = choose_color();
  leds[10] = choose_color();
  FastLED.show();
  
  /* 
   *  uncomment this to set the clock with Processing 
   *  day, month, year, dow are untested
   **/
  
//  setSyncProvider(requestSync);  //set function to call when sync required
//  while (!Serial.available()) {}
//  
//  processSyncMessage();
//  time_t t = now();
//  int hh = hour(t);
//  int mm = minute(t);
//  int ss = second(t); 
//  int dd = day(t);
//  int mth = month(t);
//  int yy = year(t);
//  int wkday = weekday(t);
//  rtc.setTime(hh, mm, ss);
//  rtc.setDate(dd, mth, yy);
//  rtc.setDOW(wkday);
  

  /* get time from rtc */
  rtc_time = rtc.getTime();
  last_h = rtc_time.hour;
  last_m = rtc_time.min;
  update_time(last_h, last_m);

  offset_set = false;
}

void loop() { 
  rtc_time = rtc.getTime();
  int hh = rtc_time.hour;
  int mm = rtc_time.min;
  int ss = rtc_time.sec;

  //add 20 seconds each time it is 12:00:00
  if (hh%12 == 0 && mm == 0 & ss == 0 && !offset_set) {
    offset_set = true;
    rtc.setTime(hh, mm, 20); //equates to missing 1 minute every 1.5 days
  }

  //return offset_set state to false for next 12:00:00
  if (hh%12 == 0 && mm == 1 && offset_set) {
    offset_set = false;
  }

  Serial.println(String(hh) + ":" + String(mm) + ":" + String(ss));
//  Serial.println("color of first LED is...");
//  Serial.println(leds[0].red);
//  Serial.println(leds[0].green);
//  Serial.println(leds[0].blue);
  
  if (hh != last_h || mm != last_m) {
    update_time(hh, mm);
  }

  // to light all LEDs
  /*for (int i = 0; i < NUM_LEDS; ++i) {
    leds[i] = CRGB(0, 190, 255);
  }
  FastLED.show();*/

//  delay(1000);
}
