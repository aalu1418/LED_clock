#include <FastLED.h>
#define NUM_LEDS 26

CRGBArray<NUM_LEDS> leds;

void setup() { FastLED.addLeds<WS2812B, 6>(leds, NUM_LEDS); }

void loop(){ 
  for(int i = 0; i < NUM_LEDS; i++) {   
    // let's set an led value
    leds[i] = CHSV(125,125,100);
  }
  FastLED.show();
}
