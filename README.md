# LED clock
A simple clock built using WS2812B LED string and an arduino uno + real-time clock.

### Description
Inspiration: https://blog.arduino.cc/2016/06/13/a-diy-digital-arduino-clock-designed-for-and-by-teachers/  

Instructions:
- For basic operation:
  - Use the  `led_time`
    - all required packages are included in this repository and must be installed in arduino
- To sync real-time clock:
  - Uncomment the following in the `led_time` file:
    ```
    //  setSyncProvider(requestSync);  //set function to call when sync required
    //  while (!Serial.available()) {}
    //  
    //  processSyncMessage();
    //  time_t t = now();
    //  int hh = hour(t);
    //  int mm = minute(t);
    //  int ss = second(t);
    //  int dd = day(t);
    //  int mth = month(t);
    //  int yy = year(t);
    //  int wkday = weekday(t);
    //  rtc.setTime(hh, mm, ss);
    //  rtc.setDate(dd, mth, yy);
    //  rtc.setDOW(wkday);
    ```
  - Using the [Processing IDE](https://processing.org/) run the `SyncArduinoClock` file.
- If the real-time clock is slow:
  - Modify the following code:
  ```
  if (hh%12 == 0 && mm == 0 & ss == 0 && !offset_set) {
    offset_set = true;
    rtc.setTime(hh, mm, 20); //equates to missing 1 minute every 1.5 days
  }
  ```
  - In this case, 20 seconds is added every 12 hours which is equivalent to the RTC clock missing 1 minute every 1.5 days.

![](./finished_clock.jpg)
_LEDs are really hard to photograph_

---
### Parts & Resources

LED (x26) - https://www.amazon.com/ALITOVE-Individual-Addressable-Programmable-Non-waterproof/dp/B01MG49QKD/
https://www.adafruit.com/product/1655
- WS2812B datasheet: https://www.kitronik.co.uk/pdf/WS2812B-LED-datasheet.pdf
- Arduino library - http://fastled.io/

Real Time Clock - https://www.amazon.com/Holdding-AT24C32-Precision-Temperature-compensated-Oscillator/dp/B00LZCTMJM/
- Arduino library - http://www.rinkydinkelectronics.com/library.php?id=73
- Arduino tutorial - https://howtomechatronics.com/tutorials/arduino/arduino-ds3231-real-time-clock-tutorial/
<!-- - RPi tutorial - https://learn.adafruit.com/adding-a-real-time-clock-to-raspberry-pi/overview
- Processing software - https://processing.org/download/
  - When syncing the RTC with computer time, first upload Arduino code, then run Processing code -->

Frosted Acrylic - https://www.walmart.com/ip/ONE-FROSTED-ACRYLIC-PLASTIC-SHEET-1-8-8-X-12/935070496

Opaque Acrylic - https://www.walmart.com/ip/2-pack-WHITE-OPAQUE-ACRYLIC-PLEXIGLASS-SHEET-1-8-8-X-12/992397936

Hardware: Nylon nut/screw (hardware store)

<!-- ---

To Do:
- fix input wires
- split 5V
- ground input end of photoresistor (need resistor)
https://www.instructables.com/id/How-to-use-a-photoresistor-or-photocell-Arduino-Tu/ -->
